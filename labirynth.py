import random
from random import choice


# Требования:
# - при создании любого прямоугольника, проверять что он корректный
# первая точка - левый верхний угол, вторая точка - правый нижний
# - как можно больше кода должно продолжать работать



class Rectangle(list):
    def __init__(self, p1, p2):
        super().__init__([p1, p2])
        x1, y1 = p1
        x2, y2 = p2
        self.t = y1
        self.b = y2
        self.r = x2
        self.l = x1
        self.bl = (self.l, self.b)
        self.tr = (self.r, self.t)
        self.br = (self.r, self.b)
        self.tl = (self.l, self.t)
        if not (x1<x2 and y1<y2):
            raise ValueError('p1 should be top-left point, p2 should be bottom-right point')
    def __eq__(self, other):
        if isinstance(other, Rectangle):
            return self.br==other.br and self.tl==other.tl
        else:
            return tuple(self)==other
    def crosses(self, other):
        for point in [self.br, self.tr, self.bl, self.tl]:
            if is_in_rectangle(point, other):
                return True
        for point in [other.br, other.tr, other.bl, other.tl]:
            if is_in_rectangle(point, self):
                return True
        (x11,y11),(x12,y12) = self
        (x21,y21),(x22,y22) = other
        if x11<=x21<=x22<=x12 and y21<=y11<=y12<=y22:
            return True
        if x21<=x11<=x12<=x22 and y11<=y21<=y22<=y12:
            return True
        return False
    
def make_point(w,h):
    x1=random.randint(0,w-1)
    y1=random.randint(0,h-1)
    return x1, y1

for _ in range(1000):
    x, y = make_point(5, 5)
    assert type(x) == int and type(y) == int and 0<=x<5 and 0<=y<5

def make_rectangle2(w,h):
    x1,y1=make_point(w-1,h-1)
    x2,y2=make_point(w-1,h-1)
    while True:
        _w, _h = abs(x2-x1), abs(y2-y1)
        if x1>x2 and y1>y2:
            if _w>=3 and _h>=3:
                return Rectangle((x2,y2),(x1,y1))
        elif x1==x2 and y1>y2:
            pass
        elif x1<x2 and y1>y2:
            if _w>=3 and _h>=3:
                return Rectangle((x1,y2),(x2,y1))
        elif x1>x2 and y1==y2:
            pass
        elif x1<x2 and y1==y2:
            pass
        elif x1==x2 and y1==y2:
            pass
        elif x1<x2 and y1<y2:
            if _w>=3 and _h>=3:
                return Rectangle((x1,y1),(x2,y2))    
        elif x1==x2 and y1<y2:
            pass
        elif x1>x2 and y1<y2:
            if _w>=3 and _h>=3:
                return Rectangle((x2,y1),(x1,y2))
        else:
            print ('нерассмотренный случай')
        x2,y2=make_point(w-1,h-1)


    
def make_field(rects, pathways):
    for i in range(40):
        for j in range(40):
            for a,rectangle in enumerate(rects):
                ((x1,y1),(x2,y2)) = rectangle
                if x1<=j<=x2 and y1<=i<=y2:
                    print(a, end='')
                    break
            else:
                for path in pathways:
                    v1, v2 = path
                    r1, r2 = rects[v1], rects[v2]
                    x1, y1 = (r1.l+r1.r)//2, (r1.t+r1.b)//2
                    x2, y2 = (r2.l+r2.r)//2, (r2.t+r2.b)//2
                    x, y = x1, y2
                    x1, x2 = min(x1, x2), max(x1, x2)
                    y1, y2 = min(y1, y2), max(y1, y2)
                    if i==y and x1<=j<=x2:
                        print('-', end='')
                        break
                    if j==x and y1<=i<=y2:
                        print('-', end='')
                        break
                else:
                    print(' ', end='')
        print()

def is_in_rectangle(point ,rect):
    x,y=point
    ((x1,y1),(x2,y2))=rect
    return x1<x<x2 and y1<y<y2


def create_rects(n):
    rects = []
    while len(rects)<n:
        rect = make_rectangle2(40, 40) # Rectangle
        for r in rects:
            if rect.crosses(r):
                break
        else:
            rects.append(rect)
    rects = [Rectangle((x1+1, y1+1), (x2-1, y2-1))
             for ((x1, y1), (x2, y2)) in rects]
    return rects



def make_graph(n):
    not_connected = list(range(n))
    connected = []
    edges = []
    vertex1 = choice(not_connected)
    not_connected.remove(vertex1)
    vertex2 = choice(not_connected)
    not_connected.remove(vertex2)
    connected.append(vertex1)
    connected.append(vertex2)
    edge = (vertex1, vertex2)
    edges.append(edge)

    while not_connected:
        vertex1 = choice(connected)
        vertex2 = choice(not_connected)
        not_connected.remove(vertex2)
        connected.append(vertex2)
        edge = (vertex1, vertex2)
        edges.append(edge)
    return edges


rects = create_rects(5)
pathways = make_graph(5)
#rects = [Rectangle((0,0), (10,10)), Rectangle((10,20), (20,30))]
#pathways = [(1,0)]
make_field(rects, pathways)








